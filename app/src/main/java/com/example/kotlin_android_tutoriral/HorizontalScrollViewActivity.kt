package com.example.kotlin_android_tutoriral

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.ImageView

class HorizontalScrollViewActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_horizontal_scroll_view)
        val image1 = findViewById<ImageView>(R.id.image1)
        val image2 = findViewById<ImageView>(R.id.image2)
        val image3 = findViewById<ImageView>(R.id.image3)
        val image4 = findViewById<ImageView>(R.id.image4)
        image1.setOnClickListener {
            setImage5(it as ImageView)
        }
        image2.setOnClickListener {
            setImage5(it as ImageView)
        }
        image3.setOnClickListener {
            setImage5(it as ImageView)
        }
        image4.setOnClickListener {
            setImage5(it as ImageView)
        }
    }

    private fun setImage5(view: ImageView) {
        val image5 = findViewById<ImageView>(R.id.image5)
        image5.setImageDrawable(view.drawable)
    }
}